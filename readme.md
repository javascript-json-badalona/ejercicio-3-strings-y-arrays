## Ejercicio 3 - Strings y arrays

Crea una función que reciba un array de palabras y que imprima por consola un análisis de cada palabra.

Por ejemplo, para un array así ['hola', 'Fotosíntesis', 'Fusible', 'yo'] debería imprimir por consola:

```
#######
Palabra 1: hola
Nº de caracteres: 4
Deletreo: h-o-l-a
La palabra es par y no empieza por mayúscula
#######
Palabra 2: Fotosíntesis
Nº de caracteres: 12
Deletreo: f-o-t-o-s-í-n-t-e-s-i-s
La palabra es par y empieza por mayúscula
#######
Palabra 3: Fusible
Nº de caracteres: 7
Deletreo: f-u-s-i-b-l-e
La palabra es impar y empieza por mayúscula
#######
Palabra 4: yo
Nº de caracteres: 2
Deletreo: y-o
La palabra es par y no empieza por mayúscula
#######
```